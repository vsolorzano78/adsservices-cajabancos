package ec.com.aseguradoradelsur;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import ec.com.aseguradoradelsur.util.YAMLConfig;
import ec.com.aseguradoradelsur.verticle.MainVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.spi.VerticleFactory;

@SpringBootApplication
@Import({KieConfiguration.class})
public class Application {

	private static final Logger LOG = LoggerFactory.getLogger(Application.class);
	
	@Autowired
	private VerticleFactory verticleFactory;
	
	@Autowired
	private YAMLConfig serverProperties;
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);		
    }
	
	@PostConstruct
	public void deployVerticle()
	{
		Vertx vertx = Vertx.vertx();
		vertx.registerVerticleFactory(verticleFactory);
		DeploymentOptions options = new DeploymentOptions().setInstances(1);		
		vertx.deployVerticle(SpringVerticleFactory.getNormalizedName(MainVerticle.class),options);
		
		LOG.info("Variables de ambiente: "+serverProperties.toString());
	}
}

package ec.com.aseguradoradelsur.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "server")

public class YAMLConfig {
	private String environment;   //Ambiente 
	private String vtWsdl;  //URL VT
	private String psIp;  //IP PS
	private String psPuerto; //Puerto PS
	private String psRs; //Recurso PS
	private Boolean sendMail; //Indicador de envio de mail
	private List<String> clusterKafka = new ArrayList<>(); //Servidores KAFKA
	
    
	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}	
		
	 public String getVtWsdl() {
		return vtWsdl;
	}

	public void setVtWsdl(String vtWsdl) {
		this.vtWsdl = vtWsdl;
	}

	public String getPsIp() {
		return psIp;
	}

	public void setPsIp(String psIp) {
		this.psIp = psIp;
	}

	public String getPsPuerto() {
		return psPuerto;
	}

	public void setPsPuerto(String psPuerto) {
		this.psPuerto = psPuerto;
	}

	public List<String> getClusterKafka() {
		return clusterKafka;
	}

	public void setClusterKafka(List<String> clusterKafka) {
		this.clusterKafka = clusterKafka;
	}	

	public String getPsRs() {
		return psRs;
	}

	public void setPsRs(String psRs) {
		this.psRs = psRs;
	}
		
	public Boolean getSendMail() {
		return sendMail;
	}

	public void setSendMail(Boolean sendMail) {
		this.sendMail = sendMail;
	}

	@Override
	 public String toString() {
	        return "Properties{" +
	                "environment=" + environment +
	                ", wsdlVT=" + vtWsdl +
	                ", ipPS=" + psIp +
	                ", puertoPS=" + psPuerto +
	                ", psRs=" + psRs +
	                ", sendMail=" + sendMail +
	                ", clusterKafka=" + clusterKafka.toString() +
	                '}';
	    }
	
    
}
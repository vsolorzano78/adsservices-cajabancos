package ec.com.aseguradoradelsur;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KieConfiguration {
	@Bean
	public KieContainer getKieContainer()
	{
		return KieServices.Factory.get().getKieClasspathContainer();
	}
}

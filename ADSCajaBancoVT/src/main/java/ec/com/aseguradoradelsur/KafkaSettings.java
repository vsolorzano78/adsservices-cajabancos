package ec.com.aseguradoradelsur;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("kafka")
public class KafkaSettings {

private String servers;
	
	private String advertisedHostName;
	
	private String offset;
	
	private String commitMs;

	public String getServers() {
		return servers;
	}

	public void setServers(String servers) {
		this.servers = servers;
	}

	public String getOffset() {
		return offset;
	}

	public void setOffset(String offset) {
		this.offset = offset;
	}

	public String getCommitMs() {
		return commitMs;
	}

	public void setCommitMs(String commitMs) {
		this.commitMs = commitMs;
	}

	public String getAdvertisedHostName() {
		return advertisedHostName;
	}

	public void setAdvertisedHostName(String advertisedHostName) {
		this.advertisedHostName = advertisedHostName;
	}
}


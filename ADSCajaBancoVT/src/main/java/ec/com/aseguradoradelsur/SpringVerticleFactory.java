package ec.com.aseguradoradelsur;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import io.vertx.core.Verticle;
import io.vertx.core.spi.VerticleFactory;

@Component
public class SpringVerticleFactory implements VerticleFactory, ApplicationContextAware {

	  private ApplicationContext applicationContext;
	  public static final String FACTORY_NAME = "ADS";
	  
	  public static String getNormalizedName(Class<?> verticle)
	  {
		  return FACTORY_NAME + ":" + verticle.getName();
	  }
	  
	  @Override
	  public boolean blockingCreate() {
	    // Usually verticle instantiation is fast but since our verticles are Spring Beans,
	    // they might depend on other beans/resources which are slow to build/lookup.
	    return true;
	  }

	  @Override
	  public String prefix() {
	    // Just an arbitrary string which must uniquely identify the verticle factory
	    return FACTORY_NAME;
	  }

	  @Override
	  public Verticle createVerticle(String verticleName, ClassLoader classLoader) throws Exception {
	    // Our convention in this example is to give the class name as verticle name
	    String clazz = VerticleFactory.removePrefix(verticleName);
	    return (Verticle) applicationContext.getBean(Class.forName(clazz));
	  }

	  @Override
	  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
	    this.applicationContext = applicationContext;
	  }
	}

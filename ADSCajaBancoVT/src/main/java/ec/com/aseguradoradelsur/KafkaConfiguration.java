package ec.com.aseguradoradelsur;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaConfiguration {

	@Autowired
	private KafkaSettings kafkaSettings;
	
	public Map<String, String> getConsumerConfiguration(String group)
	{
		
		Map<String, String> config = new HashMap<>();
		config.put("bootstrap.servers", kafkaSettings.getServers());
		config.put("key.deserializer", "org.apache.kafka.common.serialization.ByteArrayDeserializer");
		config.put("value.deserializer", "org.apache.kafka.connect.json.JsonDeserializer");
		config.put("group.id", group);
		config.put("auto.offset.reset", kafkaSettings.getOffset());
		config.put("enable.auto.commit", "false");
		config.put("auto.commit.interval.ms", kafkaSettings.getCommitMs());
		
		return config;
	}
	
	public Map<String,String> getProducerConfiguration(String group)
	{
		
		Map<String, String> config = new HashMap<>();
		config.put("bootstrap.servers", kafkaSettings.getServers());
		config.put("key.serializer", "org.apache.kafka.common.serialization.ByteArraySerializer");
		config.put("value.serializer", "org.apache.kafka.connect.json.JsonSerializer");
		config.put("compression.codec", "1"); //gzip
		config.put("group.id", group);
		config.put("auto.offset.reset", kafkaSettings.getOffset());
		//config.put("enable.auto.commit", "false");
		config.put("auto.commit.interval.ms", kafkaSettings.getCommitMs());
		//config.put("partitioner.class", "ec.com.aseguradoradelsur.CustomPartitioner");
		
		return config;
	}
}

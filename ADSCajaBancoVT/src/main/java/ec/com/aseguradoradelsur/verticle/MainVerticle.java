package ec.com.aseguradoradelsur.verticle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.com.aseguradoradelsur.KafkaConfiguration;
import ec.com.aseguradoradelsur.SpringVerticleFactory;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MainVerticle extends AbstractVerticle {

	@Autowired
	private KafkaConfiguration kafkaConfiguration;

	@Override
	public void start(Future<Void> startFuture) throws Exception {
		Future<String> filterVerticleDeployment = Future.future();

		JsonObject filterConfig = new JsonObject();
		filterConfig.put("producerConfig", kafkaConfiguration.getProducerConfiguration("adsCajaBancosProducerGroup"));
		filterConfig.put("consumerConfig", kafkaConfiguration.getConsumerConfiguration("adsCajaBancosConsumerGroup"));

		DeploymentOptions filterVerticleDeploymentOptions = new DeploymentOptions().setConfig(filterConfig)
				.setInstances(1).setWorker(true);

		
	}

}
